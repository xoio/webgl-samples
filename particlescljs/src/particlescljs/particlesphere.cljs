(ns particlescljs.particlesphere
  (:require
    [matchstick.PingpongBuffer :as pingpong]
    [thi.ng.geom.matrix :refer [M44]]
    [matchstick.RenderTarget :as rtarget]
    [thi.ng.math.core :as math]
    [thi.ng.typedarrays.core :as arrays]
    [particlescljs.globals :as globals]
    [matchstick.shaders.noise.curlnoise :as cnoise]
    [thi.ng.geom.vector :as v]
    [thi.ng.geom.gl.camera :as cam]
    [thi.ng.geom.gl.core :as gl]
    [thi.ng.geom.gl.shaders :as sh]
    [matchstick.SimpleGeometry :as sgeometry]
    [matchstick.core :as mscore]
    [thi.ng.geom.gl.webgl.constants :as glc]))
(def fbo-size 128)
;; ------------ FILL DATA ---------------
(def particleOrigins (arrays/float32 (* fbo-size fbo-size 4)))
(dotimes [i (aget particleOrigins "length")]
  (aset particleOrigins i (* (- (math/random) 0.5) 0.2)))


;; ------------- SIMULATION SHADER --------------
(def simshader
  {:vs       (str "void main() {
              vec3 c = color;
              vec3 n = normal;
              vUv = uv;"
                  "gl_Position =  model * vec4(position, 1.0);"
                  "}")

   :fs       (str
               cnoise/curlNoise
               "void main (){"
               "vec2 uv = gl_FragCoord.xy / resolution;"
               "vec4 oPos = texture2D( originTexture , vUv );"
               "vec4 pos  = texture2D( destinationTexture , vUv );"
               "vec3 vel = pos.xyz - oPos.xyz;"

               "vec3 curl = curlNoise( pos.xyz * noiseSize );"

               "vel += curl * 0.01;"
               "vel *= .97;"

               "vec3 p = pos.xyz + vel;"
               "gl_FragColor = vec4( p , 1. );"
               ;;"gl_FragColor = vec4(1.0,1.0,0.0,1.0);"
               "}")
   :uniforms {:proj               :mat4
              :model              [:mat4 M44]
              :noiseSize          [:float 0.1]
              :resolution         [:vec2 (v/vec2 js/window.innerWidth js/window.innerHeight)]
              :originTexture      [:sampler2D 1]
              :destinationTexture [:sampler2D 2]}
   :attribs  {:position :vec3
              :normal   :vec3
              :color    :vec3
              :uv       :vec2}
   :varying  {:vUv :vec2}
   :state    {:depth-test false}})

;; --------------- RENDERING SHADER -----------------
(def rendershader
  "Defines the rendering shader used in the render stage of ping-ponging
  set the texture uniform with the texture that you expect to receive from the simulation state"
  {:vs       (str "void main() {
              vec3 c = color;
              vec3 n = normal;
              vUv = uv;"
                  "vPosition = position;"
                  "vec4 pos = texture2D( tpos , position.xy );"
                  "gl_Position =  proj * view * model * vec4(pos.xyz, 1.0);"
                  "}")
   :fs       (str "void main() {
                vec2 uv = gl_FragCoord.xy / resolution;
                //gl_FragColor = vec4(uv,0.0,1.0);
                 vec4 oPos = texture2D(tpos,vUv);
                gl_FragColor = vec4(uv,0.0,1.0);
             }")

   :uniforms {:proj       :mat4
              :view       :mat4
              :model      [:mat4 M44]
              :resolution [:vec2 (v/vec2 js/window.innerWidth js/window.innerHeight)]
              :tpos       [:sampler2D 0]}
   :attribs  {:position :vec3
              :normal   :vec3
              :color    :vec3
              :uv       :vec2}
   :varying  {:vUv       :vec2
              :vPosition :vec3}
   :state    {}})

;; --------------- RENDERING SHADER -----------------

;; ---------- RENDER DRAWING ----------------
(def particleData (arrays/float32 (* fbo-size fbo-size 3)))

(def dataCounter 0)
(dotimes [a (/ (aget particleData "length") 3)]
  (aset particleData dataCounter (/ (mod a fbo-size) fbo-size))
  (aset particleData (+ dataCounter 1) (/ (math/floor (/ a fbo-size)) fbo-size))
  (set! dataCounter (+ dataCounter 3)))
;; ------------ BUILD RENDER TARGETS---------------
(def particleBuffer (pingpong/->PingpongBuffer globals/gl simshader 128))
(pingpong/prepare particleBuffer)
(pingpong/set-data particleBuffer particleOrigins)
(def particleSpec (-> (mscore/StarterSpec
                        {:position {:data particleData :size 3}})
                      (assoc :num-vertices (/ (aget particleData "length") 3))
                      (assoc :shader (sh/make-shader-from-spec globals/gl rendershader))
                      (assoc :mode glc/points)))

(def particles (-> particleSpec
                   (gl/make-buffers-in-spec globals/gl glc/dynamic-draw)))
(defn render []
  (gl/draw-with-shader
    globals/gl
    (-> particles
        (cam/apply globals/camera))))
(def timer 0)
(def output nil)
;; ----------- RENDER --------------
(defn draw [t]

  (pingpong/updateSimulation particleBuffer globals/camera)
  (mscore/reset-viewport globals/gl)
  (gl/bind (pingpong/getOutput particleBuffer) 0)
  (render)
  (gl/unbind (pingpong/getOutput particleBuffer))
  (set! timer (inc timer))

  )
