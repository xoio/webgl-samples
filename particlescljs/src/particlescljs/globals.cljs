(ns particlescljs.globals
  (:require
    [thi.ng.geom.rect :as r]
    [thi.ng.geom.gl.camera :as cam]
    [thi.ng.typedarrays.core :as arrays]
    [thi.ng.color.core :as col]
    [matchstick.camera :as mcam]
    [thi.ng.geom.gl.core :as gl]
    [thi.ng.geom.vector :as v]))

(def gl  (gl/gl-context "CANVAS"))
(def view-rect (gl/get-viewport-rect gl))
(def canvas (.querySelector js/document "#CANVAS"))
(set! view-rect (r/rect 0 0 js/window.innerWidth js/window.innerHeight))
(def camera (cam/perspective-camera {:aspect view-rect
                                     :near 1.0
                                     :far 1000.0
                                     :fov 75}))

(defn updateCameraMatrices []
  "resize the camera and canvas to make sure everything looks right"
  (aset canvas "width" js/window.innerWidth)
  (aset canvas "height" js/window.innerHeight)

  ;; set view rect
  (set! view-rect (r/rect 0 0 js/window.innerWidth js/window.innerHeight))

  ;; make sure projection matrix scaled to current window
  (let [ucam (mcam/camera-updateprojection camera (r/rect 0 0 js/window.innerWidth js/window.innerHeight))]
    (set! camera ucam))

  ;; apply default z level
  (let [ucam (cam/set-view camera {:eye (v/vec3 0 0 100)})]
    (set! camera ucam))
  )

;; add listener to ensure camera adjusts
(.addEventListener js/window "resize" updateCameraMatrices)

(updateCameraMatrices)
