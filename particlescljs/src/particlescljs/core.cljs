(ns particlescljs.core
  (:require
    [particlescljs.particlesphere :as sphere]
    [particlescljs.globals :as globals]
    [thi.ng.geom.gl.webgl.animator :as anim]
    [thi.ng.geom.gl.core :as gl]
    [thi.ng.color.core :as col]
    [thi.ng.geom.gl.webgl.constants :as glc]))
(enable-console-print!)


(anim/animate
  (fn [t frame]
    (gl/set-viewport globals/gl globals/view-rect)
    (gl/clear-color-and-depth-buffer globals/gl col/BLACK 1)
    (gl/enable globals/gl glc/depth-test)


    (sphere/draw t)
    ;;(central/draw t)

    true))
