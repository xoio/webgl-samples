(ns matchstick.SimpleGeometry
  (:require
    [matchstick.custom-geometry.Plane :as pg]
    [thi.ng.typedarrays.core :as arrays]
    [thi.ng.geom.gl.glmesh :as glm]
    [thi.ng.geom.gl.core :as gl]
    [thi.ng.geom.gl.webgl.constants :as glc]
    [thi.ng.geom.gl.camera :as cam]
    [thi.ng.geom.core :as g]
    [thi.ng.geom.gl.shaders :as sh]
    [thi.ng.geom.polyhedra :as poly]))
;; -------------- Plane -----------------
(defn init-quad
  "Creates a non-index plane/quad for the purposes of using in fbos."
  [gl]
  (let [modelPrep {:attribs      {:position {:data (arrays/float32 [-1 -1, 1 -1, -1 1 1 1]) :size 2}
                                  :uv       {:data (arrays/float32 [0 0, 1 0, 0 1, 1 1]) :size 2}}
                   :mode         glc/triangle-strip
                   :num-vertices 4}]
    (-> modelPrep (gl/make-buffers-in-spec gl glc/dynamic-draw)))
  )


(defn MakePlane
  ([width height]
   "Builds an indexed plane and returns an IndexedGLMesh of the data.
   Port from Three.js"
   (let [plane (pg/CreatePlaneVertices width height 1 1)]
     (glm/IndexedGLMesh.
       (:vertices plane)
       (:normals plane)
       (:normals plane)
       (:uvs plane)
       (arrays/float32 (* 28))
       (:indices plane)
       #{:uvs :fnorm :vnorm}
       {} (aget (:vertices plane) "length") (aget (:indices plane) "length")))
    )
  ([width height widthSegments heightSegments]
   "Builds a plane and returns an IndexedGLMesh of the data"
   (let [plane (pg/CreatePlaneVertices width height widthSegments heightSegments)]
     (glm/IndexedGLMesh.
       (:vertices plane)
       (:normals plane)
       (:normals plane)
       (:uvs plane)
       (arrays/float32 (aget (:vertices plane) "length"))
       (:indices plane)
       #{:uvs :fnorm :vnorm}
       {} (aget (:vertices plane) "length") (aget (:indices plane) "length")))
    )
  )


;; -------------- Icosahedron -----------------

(defn buildIsoVertices [scale]
  (let [[a b c d e f g h i j k l] (poly/icosahedron-vertices scale)
        isoshape   [[b a c] [c d b] [e d f] [g d e]
                    [h a i] [j a h] [k e l] [l h k]
                    [f c j] [j l f] [i b g] [g k i]
                    [f d c] [b d g] [c a j] [i a b]
                    [j h l] [k h i] [l e f] [g e k]]]
    isoshape))

(defn MakeIcosahedron
  ([gl shader-spec scale]
   "Builds an Icosahedron into a simple drawable object.
   Pass in your own shader spec for drawing. For anything more detailed,
   see the main implementation in thi.ng"
   (let [isoshape (buildIsoVertices scale)
         modelPrep (-> {:attribs      {:position {:data (arrays/float32 (flatten isoshape)) :size 3}}
                        :mode         glc/triangle-strip
                        :num-vertices (/ (count (flatten isoshape)) 3)
                        :shader        (sh/make-shader-from-spec gl shader-spec)})
         model (-> modelPrep (gl/make-buffers-in-spec gl glc/dynamic-draw))]

     model))
  ([gl shader-spec]
   "Builds an Icosahedron into a simple drawable object.
   Pass in your own shader spec for drawing. For anything more detailed,
   see the main implementation in thi.ng"
   (let [isoshape (buildIsoVertices 3)
         modelPrep (-> {:attribs      {:position {:data (arrays/float32 (flatten isoshape)) :size 3}}
                        :mode         glc/triangle-strip
                        :num-vertices (/ (count (flatten isoshape)) 3)
                        :shader        (sh/make-shader-from-spec gl shader-spec)})
         model (-> modelPrep (gl/make-buffers-in-spec gl glc/dynamic-draw))]

     model))


  )