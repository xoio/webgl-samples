(ns matchstick.buffer
  (:require
    [matchstick.shaders.fx-shader :as fx-shader]
    [matchstick.SimpleGeometry :as sgeometry]
    [matchstick.core :as mscore]
    [thi.ng.geom.core :as g]
    [thi.ng.geom.gl.buffers :as buf]
    [thi.ng.geom.gl.webgl.constants :as glc]
    [thi.ng.geom.gl.core :as gl]
    [thi.ng.typedarrays.core :as arrays]
    [thi.ng.geom.gl.fx :as fx]
    [thi.ng.geom.gl.shaders :as sh]))

(defn MakePingpongFbos
  ([gl origindata origindatacopy]
   (let [size 128
         fbo1 (buf/make-fbo-with-attachments gl {:width size
                                                 :height size
                                                 :tex origindata
                                                 :depth? false})
         fbo2 (buf/make-fbo-with-attachments gl {:width size
                                                 :height size
                                                 :tex origindatacopy
                                                 :depth? false})]
     {:fbos [fbo1 fbo2]
      :textures [origindata origindatacopy]}
     )))



(defn DataTexture
  "Constructs a texture populated with number data"
  ([gl data]
   (let [size 128
         texture (buf/make-texture gl {:width  size
                                       :height size
                                       :filter glc/linear
                                       :wrap glc/clamp-to-edge
                                       :format glc/rgba
                                       :pixels (or (if (vector? data) (arrays/float32 (flatten data))) data)})]
     texture
     )))


(defn DataTextureFloat
  "Constructs a texture populated with floating point number data"
  ([gl data]
    ;; first enable floating point textures
   (.getExtension gl "OES_texture_float")
   (.getExtension gl "OES_float_linear")
   (let [size 128
         texture (mscore/make-texture gl {:width  size
                                          :height size
                                          :filter glc/nearest
                                          :wrap glc/clamp-to-edge
                                          :format glc/rgba
                                          :type glc/float
                                          :pixels data})]
     texture
     )

    ))

;;        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA,212,212,0,gl.RGBA, gl.FLOAT,array);