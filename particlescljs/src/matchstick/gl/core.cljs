(ns matchstick.gl.core
  (:require [thi.ng.geom.gl.core :as gl]
            [thi.ng.geom.gl.webgl.constants :as glc]))

(defn UpdateBuffer [gl spec id coll]
  "Updates the data of the specified attribute in a model spec.
  Takes a context, a spec map, the id of the attribute to update and finally
  the data used in the update."
  (let [{:keys [target data buffer buffer-mode size]} (-> (get spec :model) :attribs id)]
    (gl/fill-vertex-buffer data coll size)
    (.bindBuffer gl target buffer)
    (.bufferData gl target data buffer-mode))
  )

(defn EnableWireframe [spec]
  "Enables wireframe mode for the specified spec. Returns a
  new spec."
  (assoc-in spec [:mode] glc/line-loop))