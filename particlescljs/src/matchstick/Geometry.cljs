(ns matchstick.Geometry
  (:require
    [thi.ng.typedarrays.core :as tarrays]
    [thi.ng.geom.gl.shaders :as sh]
    [thi.ng.typedarrays.core :as arrays]
    [thi.ng.geom.gl.glmesh :as glmesh]
    [thi.ng.geom.gl.webgl.constants :as glc]
    [thi.ng.geom.gl.core :as gl]
    [thi.ng.geom.core :as g]
    [thi.ng.geom.matrix :as mat :refer [M44]]
    [thi.ng.geom.gl.camera :as cam]))


;; -------------- PROTOCOL -------------------
(defprotocol IGeometry
  "Protocol that provides a thin abstraction layer around thi.ng GLMesh to make adjusting to clojure a bit easier
  for OOP folks"
  (init [this] "basic initialization - ensures that the attribute map for the object gets created")
  (addAttribute [this attrib] "adds an attribute to the geometry. Takes a name, the typed array
  data and finally the size for each vertex")
  (setNumVertices [this num] "Sets the number of vertices to draw for the object")
  (setUsage [this usage] "Sets the usage mode - gl.DYNAMIC_DRAW, gl.STATIC_DRAW etc")
  (setMode [this mode] "sets the drawing mode for the geometry, IE gl.TRIANGLES, gl.TRIANGLE_STRIP etc")
  (setShaderSpec [this shaderspec] "Sets the shader spec for the object")
  (updateUniform [this key value] "updates a uniform value using the provided key in the :<keyname> format")
  (compile [this] "Prepares geometry for drawing")
  )
;; -------------- INDEXED GEOMETRY -------------------

(defrecord IndexedGeometry []
  IGeometry
  (init [this]))
;; -------------- REGULAR GEOMETRY -------------------
;; Geometry - takes a canvas_id prop which should be the dom id of the
;; canvas element you're derriving a context from
(defrecord Geometry [canvas_id]
  IGeometry
  (init [this]
    (aset this "usage" glc/dynamic-draw)
    (aset this "attributes" {:attribs    {}
                             :mode glc/triangle-strip
                             :num-vertices 20
                             :shader  {}}))

  (updateUniform [this key value])

  (setMode [this mode]
    (let [attributes (aget this "attributes")
          attribmap  (assoc-in attributes [:mode] mode)]
      (aset this "attributes" attribmap) this)
    )
  (setNumVertices [this num]
    (let [attributes (aget this "attributes")
          attribmap  (assoc-in attributes [:num-vertices] num)]
      (aset this "attributes" attribmap) this))
  (setShaderSpec [this spec]
    (let [attributes (aget this "attributes")
          attribmap  (assoc-in attributes [:shader] (sh/make-shader-from-spec (gl/gl-context canvas_id) spec))]
      (aset this "attributes" attribmap) this))

  (addAttribute [this attrib]
    "Adds an attribute to the object"
    (let [attributes (aget this "attributes")
          attribmap (assoc-in attributes [:attribs] attrib)]
        (aset this "attributes" attribmap))
      this)

  (compile [this]
    ;; first ensure that a model matrix is built, if not then create one
    (let [spec (aget this "attributes")]
      (if (= (get spec [:uniforms :model]) nil)
        (assoc-in spec [:uniforms :model] (-> M44))))

    (aset this "model" (-> (aget this "attributes") (gl/make-buffers-in-spec (gl/gl-context canvas_id) (aget this "usage")))) this)
  )
;; ------------ FUNCTIONS --------------
(defn MakeGeometry
  ([]
   "Initializes empty mesh"
   (let [obj (Geometry. "CANVAS")]
     (init obj)))

  ([canvas_id & attribs]
   "Creates mesh with an arbitrary number of attributes"
   (let [obj (Geometry. canvas_id)]
     (init obj)
     (doseq [attrib attribs]
       (if (map? attrib)
         (addAttribute obj attrib)
         (throw (js/Error. "Unable to run MakeMesh - attribute param is not a map"))))
     obj))
  )

