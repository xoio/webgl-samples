(ns matchstick.camera
  (:require
    [thi.ng.geom.core :as g]
    [thi.ng.geom.matrix :as mat :refer [M44]]
    [thi.ng.geom.gl.camera :as cam]))

(defn camera-translate [camera translateVector]
  "Helper function to make translation of the camera a little more understandable"
  (let [ucam (cam/set-view camera {:eye translateVector})]
    ucam))

(defn camera-updateprojection [camera view-rect]
  "Sets a new projection matrix based on the specified viewport rect"
  (let [c (cam/set-projection camera {:aspect view-rect})]
    c))