(ns matchstick.DataTexture
  (:require
    [thi.ng.typedarrays.core :as arrays]
    [thi.ng.geom.gl.webgl.constants :as glc]
    [thi.ng.geom.gl.buffers :as buf]
    [matchstick.core :as mscore]))

(defn DataTexture
  "Constructs a texture populated with number data.
  Accepts data in either the form of a TypedArray or a vector of values."
  ([gl data]
   (let [size 128
         texture (buf/make-texture gl {:width  size
                                       :height size
                                       :filter glc/linear
                                       :wrap glc/clamp-to-edge
                                       :format glc/rgba
                                       :pixels (or (if (vector? data) (arrays/float32 (flatten data))) data)})]
     texture
     )))



(defn DataTextureFloat
  "Constructs a texture populated with floating point number data.
   Accepts data in either the form of a TypedArray or a vector of values."
  ([gl data]
    ;; first enable floating point textures
   (.getExtension gl "OES_texture_float")
   (.getExtension gl "OES_float_linear")
   (let [size 128
         texture (mscore/make-texture gl {:width  size
                                          :height size
                                          :filter glc/nearest
                                          :wrap   glc/clamp-to-edge
                                          :format glc/rgba
                                          :type   glc/float
                                          :pixels (or (if (vector? data) (arrays/float32 (flatten data))) data)})]
     texture
     )))

