(ns matchstick.shaders.simulation
  (:require
    [thi.ng.geom.matrix :refer [M44]]
    [matchstick.shaders.noise.curlnoise :as cnoise]
    [thi.ng.geom.vector :as v]))

(def simulation-shader
  "Defines a complete basic simulation shader spec"
  {:vs ""
   :fs ""
   :uniforms {:proj               :mat4
              :model              [:mat4 M44]
              :noiseSize          [:float 0.1]
              :resolution         [:vec2 (v/vec2 js/window.innerWidth js/window.innerHeight)]
              :originTexture      [:sampler2D 1]
              :destinationTexture [:sampler2D 2]}
   :attribs  {:position :vec3
              :normal   :vec3
              :color    :vec3
              :uv       :vec2}
   :varying  {:vUv :vec2}
   :state    {:depth-test false}})