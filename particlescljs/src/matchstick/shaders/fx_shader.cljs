(ns matchstick.shaders.fx-shader
  (:require
    [thi.ng.geom.matrix :as mat :refer [M44]]
    [thi.ng.geom.gl.shaders :as sh]))

(def fx-passthru
  (str "void main() {
          vec3 c = color;
          vec3 n = normal;
          vUv = uv;"
          "gl_Position =  model * vec4(position, 1.0);"
       "}"))


(defn FxSpec
  "Builds a shader specification tuned for VFX / GPU stuff"
  ([^WebglRenderingContext gl vertex fragment]
   "Builds a spec for visuals. Takes a context, vertex shader and fragment shader as parameters"
   (let [spec {:vs vertex
               :fs fragment
               :uniforms {:proj               :mat4
                          :model              [:mat4 M44]
                          :noiseSize          [:float 0.1]
                          :resolution         :vec2
                          :originTexture      [:sampler2D 1]
                          :destinationTexture [:sampler2D 2]}
               :attribs  {:position :vec3
                          :normal   :vec3
                          :color    :vec3
                          :uv       :vec2}
               :varying  {:vUv :vec2}
               :state    {:depth-test false}}]
     spec))

  ([gl vertex fragment & uniforms]
   "Builds a default spec for FX shaders and accepts, in addition
   to context, vertex and fragment shaders, a map of additional uniforms to add"
   (let [newUniforms (atom {:proj               :mat4
                           :model              [:mat4 M44]
                           :noiseSize          [:float 0.1]
                           :resolution         :vec2
                           :originTexture      [:sampler2D 1]
                           :destinationTexture [:sampler2D 2]})]

     ;; append additional uniforms
     (doseq [uniform uniforms]
       (let [a (merge @newUniforms uniform)]
         (reset! newUniforms a)))

     ;; build and return spec
     (let [spec {:vs vertex
                 :fs fragment
                 :uniforms @newUniforms
                 :attribs  {:position :vec3
                            :normal   :vec3
                            :color    :vec3
                            :uv       :vec2}
                 :varying  {:vUv :vec2}
                 :state    {:depth-test false}}]
       spec)
     ))

  )