(ns matchstick.shaders.basicshaders
  (:require
    [thi.ng.geom.matrix :refer [M44]]
    [matchstick.shaders.shadervars :as svars]))



;;---------------- BASIC PASSTHRU VERTEX SHADER ----------------------

;; basic passthru shader
(def basic-passthru
  (str "void main() {
         vec3 c = color;
         vec3 n = normal;
         vec2 u = uv;"
       svars/passthru_position
       "}"))



;;---------------- BASIC COLOR SHADER ----------------------
(def basic-shader
  "Defines a complete basic color shader spec"
  {:vs (str "void main() {
              vec3 c = color;
              vec3 n = normal;
              vec2 u = uv;"
              svars/passthru_position
              "}")
   :fs (str "void main() {
                gl_FragColor = vec4(1.0,1.0,0.0,1.0);
             }")
   :uniforms {:proj :mat4
              :view :mat4
              :time :float
              :ocolor :vec3
              :model [:mat4 M44]}
   :attribs  {:position :vec3
              :normal :vec3
              :color :vec3
              :uv :vec2}
   :varying  {}
   :state    {}})

