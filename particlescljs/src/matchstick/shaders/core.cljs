(ns matchstick.shaders.core
  (:require
    [thi.ng.glsl.core :as glsl]
    [thi.ng.geom.gl.shaders :as sh]
    [thi.ng.geom.gl.webgl.constants :as glc]
    [thi.ng.geom.gl.core :as gl]
    [thi.ng.geom.gl.buffers :as buf]))

;; -----------------------------------
;; Defines some helper functions to abstract the clojure-ness
;; out from the calls and hopefully help make things less confusing
;; ---------- FUNCTIONS ---------------
(enable-console-print!)

(defn AssembleShader
  ([gl spec]
   (sh/make-shader-from-spec gl spec)
    ))

(defn AddUniforms [spec uniformMap]
  "Updates/adds new uniforms to the spec and returns a copy"
  (let [uniformed (update-in spec [:uniforms] merge uniformMap)]
    uniformed))
