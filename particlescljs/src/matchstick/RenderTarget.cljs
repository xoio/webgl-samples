(ns matchstick.RenderTarget
  (:require
    [matchstick.core :as mscore]
    [thi.ng.geom.gl.core :as gl]
    [thi.ng.geom.gl.buffers :as buf]
    [matchstick.DataTexture :as dtex]))


;; ============== RENDER TARGET -==================
;; wraps a fbo and a texture into one object for easier use
;; especially when ping-ponging
(defrecord RenderTarget [gl texture fbo size]
  Object
  (viewport [this]
    (gl/set-viewport gl 0 0 size size))

  gl/IBind
  (bind [this]
    "Binds the fbo for drawing"
    (gl/bind fbo))
  (bind [this unit]
    "Binds the texture of the render target. By
    default, will bind to unit 0 but can pass in
    a unit as well"
    (gl/bind texture (or unit 0)))

  (unbind [this]
    "Unbinds both the texture and the fbo"
    (gl/unbind texture)
    (gl/unbind fbo))
  )




;; ============== FUNCTIONAL DECLARATIONS -==================
(defn make-rendertarget
  ([^WebGLRenderingContext gl toptions]
   "Builds a RenderTarget. Takes a WebGLRenderingContext and a Map of options for the texture."
   (let [texture (mscore/make-texture gl toptions)
         fbo (buf/make-fbo-with-attachments gl {:tex texture
                                                :width 128
                                                :height 128
                                                :depth? false})]
     (->RenderTarget gl texture fbo 128))
    )
  ([^WebGLRenderingContext gl toptions size]
   "Builds a RenderTarget. Takes a WebGLRenderingContext and a Map of options for the texture.
   Also takes a size for sizing the fbo and texture."
   (let [texture (mscore/make-texture gl toptions)
         fbo (buf/make-fbo-with-attachments gl {:tex texture
                                                :width size
                                                :height size
                                                :depth? false})]
     (->RenderTarget gl texture fbo size))
    ))
;; ------------- FLOATING POINT --------------------
(defn make-floating-point-rendertarget
  ([^WebGLRenderingContext gl data]
   "Builds a RenderTarget with floating point data. Takes a WebGLRenderingContext and a Map of options for the texture."
   (let [texture (dtex/DataTextureFloat gl data)
         fbo (buf/make-fbo-with-attachments gl {:tex texture
                                                :width 128
                                                :height 128
                                                :depth? false})]
     (->RenderTarget gl texture fbo 128))
    )
  ([^WebGLRenderingContext gl data size]
   "Builds a RenderTarget with floating point data. Takes a WebGLRenderingContext and a Map of options for the texture.
   Also takes a size for sizing the fbo and texture."
   (let [texture (dtex/DataTextureFloat gl data)
         fbo (buf/make-fbo-with-attachments gl {:tex texture
                                                :width size
                                                :height size
                                                :depth? false})]
     (->RenderTarget gl texture fbo size))
    ))

