(ns matchstick.PingpongBuffer
  (:require
    [matchstick.RenderTarget :as rtarget]
    [thi.ng.geom.gl.shaders :as sh]
    [matchstick.SimpleGeometry :as sgeometry]
    [thi.ng.geom.gl.core :as gl]))

(defprotocol IGPUBuffer
  "A protocol for objects designed to do GPU ping-ponging"
  (prepare [this] "prepares the simulation by creating a quad and building the simulation shader")
  (set-data [this arraydata] "Sets the origin data for all the vertices in the simulation")
  (getOutput [this] "Returns the current output of the calculations")
  (updateSimulation [this camera] "updates the simulation"))


(defrecord PingpongBuffer [^WebGLRenderingContext gl simulation fbo-size]
  IGPUBuffer
  (prepare[this]
    "Prepares the simulation by creating a drawing quad for the object."
    (aset this "quad" (-> (sgeometry/init-quad gl)
                          (assoc :shader (sh/make-shader-from-spec gl simulation))))
    (aset this "counter" 0))

  (set-data [this arraydata]
    "Sets the initial data for the buffer"
    (let [rt1 (rtarget/make-floating-point-rendertarget gl arraydata fbo-size)
          rt2 (rtarget/make-floating-point-rendertarget gl arraydata fbo-size)
          rt3 (rtarget/make-floating-point-rendertarget gl arraydata fbo-size)]
      ;; prepare output prop
      (aset this "output" (atom 0))
      (aset this "targets" [rt1 rt2 rt3]))
    )

  (getOutput [this]
    "Returns the current output from the simulation"
    @(aget this "output"))

  (updateSimulation [this camera]
    (let [counter (aget this "counter")
          quad (aget this "quad")
          flipFlop (mod counter 3)
          targets (aget this "targets")
          output (aget this "output")
          rt1 (get targets 0)
          rt2 (get targets 1)
          rt3 (get targets 2)]

      (if (= flipFlop 0)
        (do
          (gl/bind rt3)
          (doto gl
            (gl/set-viewport 0 0 fbo-size fbo-size))
          (gl/bind rt1 1)
          (gl/bind rt2 2)
          (gl/draw-with-shader gl quad)
          (gl/unbind rt1)
          (gl/unbind rt2)
          (gl/unbind rt3)
          (reset! output (:texture rt3))
          ))

      (if (= flipFlop 1)
        (do
          (gl/bind rt1)
          (doto gl
            (gl/set-viewport 0 0 fbo-size fbo-size))
          (gl/bind rt2 1)
          (gl/bind rt3 2)
          (gl/draw-with-shader gl quad)
          (gl/unbind rt2)
          (gl/unbind rt3)
          (gl/unbind rt1)
          (reset! output (:texture rt1))
          ))


      (if (= flipFlop 2)
        (do
          (gl/bind rt2)
          (doto gl
            (gl/set-viewport 0 0 fbo-size fbo-size))
          (gl/bind rt3 1)
          (gl/bind rt1 2)
          (gl/draw-with-shader gl quad)
          (gl/unbind rt3)
          (gl/unbind rt1)
          (gl/unbind rt2)
          (reset! output (:texture rt2))
          ))



      (aset this "counter" (+ 1 counter))
      )
    )


  )
