(ns matchstick.custom-geometry.simple-envmap
  (:require
    [thi.ng.math.core :as math]
    [matchstick.core :as mc]
    [thi.ng.geom.vector :as vec]))

;; Simple env map for demos - NOT a real cubemap
;; using technique as discussed by Yi-Wen
;; http://blog.bongiovi.tw/simple-environment-map/


;; ---------------- SETUP -----------------
(def numSegments 24)
(def size 1800)

(defn getPosition
  ([i j isNormal]
   (let [rx (- (* (/ i numSegments) math/PI) (* math/PI 0.5))
         ry (* (/ j numSegments) math/PI 2)
         r 1]

     (let [x (* (js/Math.sin rx) r)
           t (* (js/Math.cos rx) r)
           y (* (js/Math.cos ry) t)
           z (* (js/Math.sin ry) t)]
       (vec/vec3 x y z))
     ))
  ([i j]
   (let [rx (- (* (/ i numSegments) math/PI) (* math/PI 0.5))
         ry (* (/ j numSegments) math/PI 2)
         r size]

     (let [x (* (js/Math.sin rx) r)
           t (* (js/Math.cos rx) r)
           y (* (js/Math.cos ry) t)
           z (* (js/Math.sin ry) t)]
       (vec/vec3 x y z))
     ))
  )

(defn GenerateEnvMap []
  (let [positions (atom [])
        indices (atom [])
        coords (atom [])
        index (atom 0)
        gapUV (/ 1 numSegments)]

    (dotimes [i numSegments]
      (dotimes [j numSegments]

        (let [pos1 (getPosition i j)
              pos2 (getPosition (+ 1 i) j)
              pos3 (getPosition (+ i 1) (+ j 1))
              pos4 (getPosition i (+ j 1))
              u (/ j numSegments)
              v (/ i numSegments)
              coords1 (vec/vec2 (- 1.0 u) v)
              coords2 (vec/vec2 (- 1.0 u) (+ v gapUV))
              coords3 (vec/vec2 (- 1.0 u gapUV) (+ v gapUV))
              coords4 (vec/vec2 (- 1.0 u gapUV) v)
              indices1 (+ (* index 4) 3)
              indices2 (+ (* index 4) 2)
              indices3 (+ (* index 4) 0)
              indices4 (+ (* index 4) 2)
              indices5 (+ (* index 4) 1)
              indices6 (+ (* index 4) 0)]

          (reset! positions
                  (conj @positions pos1)
                  (conj @positions pos2)
                  (conj @positions pos3)
                  (conj @positions pos4))

          (reset! coords
                  (conj @coords coords1)
                  (conj @coords coords2)
                  (conj @coords coords3)
                  (conj @coords coords4))

          (reset! indices
                  (conj @indices indices1)
                  (conj @indices indices2)
                  (conj @indices indices3)
                  (conj @indices indices4)
                  (conj @indices indices5)
                  (conj @indices indices6))
          ;; indcrement counter
          (reset! index (inc index))
          )
        ))
    {:positions positions
     :indices indices
     :uvs coords})

  )
;; ---------------- MESH VAR SETUP -----------------
(def MapSpec (mc/StarterSpec))
;; ---------------- SHADER SETUP-----------------
(def vertex
  "void main(){}")
(def fragment
  "void main(){}")

;; init spec
(def shader (mc/StarterShaderSpec))

;; setup uniforms
(def uniforms {:map :sampler2D})
(set! shader (update-in shader [:uniforms] merge uniforms))
(set! shader (assoc-in shader [:vs] vertex))
(set! shader (assoc-in shader [:fs] fragment))



