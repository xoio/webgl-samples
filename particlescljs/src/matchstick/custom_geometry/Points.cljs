(ns matchstick.custom-geometry.points
  (:require
    [matchstick.core :as mscore]
    [thi.ng.geom.gl.shaders :as sh]
    [thi.ng.geom.gl.webgl.constants :as glc]
    [thi.ng.geom.gl.core :as gl]))


(defn MakePoints [gl particleData shader]
  "Creates a spec for making particles.
  Takes a WebGLRenderingContext, a typed array of data and a shader for
  rendering."
  (let [particleSpec (-> (mscore/StarterSpec
                           {:position {:data particleData :size 3}})
                         (assoc :num-vertices (/ (aget particleData "length") 3))
                         (assoc :shader (sh/make-shader-from-spec gl shader))
                         (assoc :mode glc/points))]

    (-> particleSpec
        (gl/make-buffers-in-spec gl glc/dynamic-draw))))
