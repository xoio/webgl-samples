//constants and other globally necessary things
import {randVec3} from "./math/Core"
import Vector from "./math/Vector"
window.noiseSpeed =  0.001
window.noiseScale =  1200
window.noiseSeparation =  0.1
window.ribbonSpeed =  1
window.usePostProc =  true
window.showBounds =  true
window.autoRotate =  true
window.ribbonWidth =  6
window.startRange =  100
window.clumpiness =  0.8

window.ribbons = [];
window.emitters = [];

window.BOUNDS = 200;
window.EMITTER_COUNT = 3;
window.RIBBON_COUNT = 600;

window.noise = new SimplexNoise();
window.noiseTime = Math.random()*1000;
window.gvec = new Vector();
window.tangent = new Vector();
window.normal = new Vector();
window.up = new Vector(1,1,1);

//CREATE EMITTERS
for (var i = 0; i < EMITTER_COUNT; i++) {
    let newVec = randVec3(window.BOUNDS/2);
    var v = new Vector(newVec[0],newVec[1],newVec[2]);

    emitters[i] = v
}

window.debug = false;