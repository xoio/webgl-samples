import {randomRange,map,constrain} from "./math/Core"
import Vector from "./math/Vector"

class FlowField {
    constructor(){
        this.resolution = 20;
        this.width = 800;
        this.height = 800;
        this.depth = 800;

        this.cols = this.width / 10;
        this.rows = this.height / 10;
        this.dep = this.depth / 10;


        this._initField();
        this._buildField();
    }

    _initField(){
        var field = [];

        for(var i = 0; i < this.cols;++i){
            var rField = [];
            for(var j = 0; j < this.rows; ++j){

                var dField = [];
                for(var p = 0; p < this.dep;++p){
                    dField.push(0);
                }
                rField.push(dField);
            }

            field.push(rField);
        }

        this.field = field;
    }

    _buildField(){

        var xoff = 0.0
        for(var i = 0; i < this.cols;++i){
            var yoff = 0.0
            for(var j = 0; j < this.rows; ++j){
                for(var p = 0; p < this.dep;++p){

                    let theta = map(randomRange(xoff,yoff),0,1,0,3.14149 * 3.14149);

                    var v = new Vector(Math.cos(theta),Math.sin(theta),Math.random());

                    this.field[i][j][p] = v;

                }
                yoff += 0.1

            }

          xoff += 0.1;
        }

        console.log(this.field);
    }

    lookup(lookup){
        let column = (constrain(lookup.x/this.resolution,0,this.cols-1));
        let row = (constrain(lookup.y/this.resolution,0,this.rows-1));
        let dep = (constrain(lookup.z/this.resolution,0,this.depth-1));

        column = Math.ceil(column);
        row = Math.ceil(row);
        dep = Math.ceil(dep);

        return this.field[column][row][dep];
    }
}

export default FlowField;