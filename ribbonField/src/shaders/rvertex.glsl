attribute vec3 position;
attribute vec3 color;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

void main()
{
    mat4 modelViewMatrix = modelMatrix * viewMatrix;
	// simply pass position and vertex color on to the fragment shader
	gl_Position = projectionMatrix * modelViewMatrix * vec4(position,1.0);
}