attribute vec3 position;
attribute vec3 color;
attribute vec2 uv;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

varying vec2 vUv;
void main()
{
    vUv = uv;
    mat4 modelViewMatrix = modelMatrix * viewMatrix;
	// simply pass position and vertex color on to the fragment shader

    gl_PointSize = 10.0;
	gl_Position = projectionMatrix * modelViewMatrix * vec4(position,1.0);
}