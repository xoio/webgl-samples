const createBuffer = require('gl-buffer');
const glslify = require('glslify');
const createVao = require('gl-vao');
const createShader = require('gl-shader');
const pack = require('array-pack-2d');

import {randomRange,randomInt,randVec3,flattenArray} from "./math/Core"
import Matrix4 from "./math/Matrix4"
import Vector from "./math/Vector"

class TestObject {
    constructor(gl){
        this.texCoordBuffer = createBuffer(gl,new Float32Array(),gl.ARRAY_BUFFER,gl.STATIC_DRAW);
        this.buffer = createBuffer(gl, new Float32Array(), gl.ARRAY_BUFFER, gl.STATIC_DRAW);
        this.shader = createShader(gl,glslify('./shaders/core/vertex.glsl'),glslify('./shaders/core/fragment.glsl'));
        this.vao = createVao(gl);

        this.shader.attributes.position.location = 0;
        this.shader.attributes.uv.location = 1;

        this.buffer.update(new Float32Array(
            [
                1.0,  1.0,  0.0,
                -1.0,  1.0,  0.0,
                1.0, -1.0,  0.0,
                -1.0, -1.0,  0.0
            ]
        ));

        this.texCoordBuffer.update(new Float32Array(
            [
                0.0, 0.0,
                1.0, 0.0,
                1.0, 1.0,
                0.0, 1.0,
            ]
        ))

        this.modelMatrix = new Matrix4();
        this.vao.update([
            {
                buffer:this.buffer,
                size:3
            },
            {
                buffer:this.texCoordBuffer,
                size:2
            }
        ]);

        this.modelMatrix.translate([0,0.0,-8.0]);
        this.gl = gl;
    }

    draw(camera,texture){
        this.shader.bind();

        this.shader.uniforms.projectionMatrix = camera.projection;
        this.shader.uniforms.viewMatrix = camera.view;
        this.shader.uniforms.modelMatrix = this.modelMatrix.get();
        this.shader.uniforms.texture = texture.bind();

        this.vao.bind();
        this.vao.draw(this.gl.TRIANGLE_STRIP,4);
    }
}

export default TestObject