/**
 * Clears the context
 * @param gl a webgl context
 * @param clearColor a optional color in array format to use as teh clear color
 */
export function clear(gl,clearColor=[0,0,0,1]){
    gl.clearColor(clearColor[0],clearColor[1],clearColor[2],clearColor[3]);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

}

/**
 * Enables a component on the context
 * @param gl
 * @param flag
 */
export function enable(gl,flag){
    if(flag instanceof Number){
        gl.enable(flat);
    }
}
