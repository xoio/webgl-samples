/**
 * A simple Vector class
 */
class Vector {
    constructor(x=0,y=0,z=0){
        this.x = x;
        this.y = y;
        this.z = z;
    }
    getArray(){
        return [this.x,this.y,this.z];
    }
    /**
     * Adds two vectors together
     * @param vec another Vector object
     */
    add(vec){
        if(vec.hasOwnProperty("x")){
            this.x += vec.x;
            this.y += vec.y;
            this.z += vec.z;
        }else if (vec instanceof Array){
            this.x += vec[0]
            this.y += vec[1]
            this.z += vec[2]
        }

        return this;
    }

    /**
     * Subtracts a vector's values from this one
     * @param vec the other vector
     */
    subtract(vec){
        this.x -= vec.x;
        this.y -= vec.y;
        this.z -= vec.z;

        return this;
    }

    copy(vec){
        this.x = vec.x;
        this.y = vec.y;
        this.z = vec.z;
        return this;
    }


    /**
     * Divides this vector by another
     * @param vec the other vector
     */
    divide(vec){
        if(vec.hasOwnProperty("x")){
            this.x /= vec.x;
            this.y /= vec.y;
            this.z /= vec.z;
        }else if (vec instanceof Array){
            this.x /= vec[0]
            this.y /= vec[1]
            this.z /= vec[2]
        }

        return this;
    }

    addVectors(vec1,vec2){
        vec1.add(vec2);

        this.x = vec1.x;
        this.y = vec1.y;
        this.z = vec1.z;

        return this;
    }

    /**
     * Normalizes the Vector's values
     */
    normalize(){

        var len = this.x * this.x + this.y * this.y + this.z * this.z;
        if (len > 0) {
            len = 1 / Math.sqrt(len);
            this.x *= len;
            this.y *= len;
            this.z *= len;
        }

        return this;
    }


    length(){
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z)
    }

    crossVectors(a,b){
        var ax = a.x, ay = a.y, az = a.z;
        var bx = b.x, by = b.y, bz = b.z;

        this.x = ay * bz - az * by;
        this.y = az * bx - ax * bz;
        this.z = ax * by - ay * bx;
        return this;
    }

    /**
     * Calculates the dot product between this vec
     * and another one.
     * @param vec another vec
     */
    dot(vec){
        return this.x * vec.x + this.y * vec.y + this.z * vec.z;
    }

    /**
     * Calculates the length of a vector
     * @returns {number} the length
     */
    length(){
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    }

    /**
     *  Divides the vector by a singular value.
     */
    divideScalar(value){
        this.x /= value;
        this.y /= value;
        this.z /= value;
        return this;
    }
    /**
     *  Multiplies the vector by a singular value.
     */
    multiplyScalar(value){
        this.x *= value;
        this.y *= value;
        this.z *= value;
        return this;
    }

    /**
     *  Adds a singular value onto the vector
     */
    addScalar(value){
        this.x += value;
        this.y += value;
        this.z += value;
        return this;
    }

    /**
     *  Subtracts the value from the vector.
     */
    subtractScalar(value){
        this.x -= value;
        this.y -= value;
        this.z -= value;
        return this;
    }

    /**
     * Sets the value of the vertex to the value from the result of
     * subtracting the first vector from the second vector
     * @param vec1 the first vector in the process
     * @param vec2 the second vector in the process
     */
    subVectors(a,b){
        if(a.hasOwnProperty('x') && b.hasOwnProperty('x')){
            this.x = a.x - b.x;
            this.y = a.y - b.y;
            this.z = a.z - b.z;
        }else if(a instanceof Array && b instanceof Array){
            a[0] -= b[0];
            a[1] -= b[1];
            a[2] -= b[2];

            this.x = a[0];
            this.y = a[1];
            this.z = a[2];
        }
        return this;
    }

}

export default Vector;