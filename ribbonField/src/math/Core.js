//various useful general functions taken from a variety of places.
// https://www.airtightinteractive.com/demos/js/noise-ribbons/lib/atutil.js
// https://github.com/yiwenl/Christmas_Experiment_2015/blob/master/src/js/SceneApp.js

export function randomRange(min, max) {
    return min + Math.random() * (max - min);
}

export function randomInt(min,max){
    return Math.floor(min + Math.random() * (max - min + 1));
}

export function map(value, min1, max1, min2, max2) {
    return lerp( norm(value, min1, max1), min2, max2);
}

export function lerp(val,min,max){
    return min + (max -min) * val;
}

export function norm(value,min,max){
    return (value - min) / (max - min);
}

/**
 * Converts degrees to radians
 * @param deg the value in degrees to convert
 * @returns {number} the value in radians
 */
export function toRadians(deg){
    return (deg * Math.PI) / 180;
}
/**
 * Convert a radian value to degrees
 * @param rad a radian value
 * @returns {number} the value in degrees
 */
export function toDegrees(rad){
    return (rad * 180 / Math.PI);
}

/**
 * Generates a random vector 3 within a range. Will return a THREE.Vector3 if available, otherwise
 * returns a array.
 * @param range
 * @returns {*}
 */
export function randVec3(range){
    if(window.THREE !== undefined){
        return new THREE.Vector3(randomRange(-range,range),randomRange(-range,range),randomRange(-range,range));
    }else{
        return [randomRange(-range,range),randomRange(-range,range),randomRange(-range,range)];
    }
}

export function slerp(x,y,p){
    return x + (y - x) * p;
}


/**
 * Flattens an array of objects presumed to be either vectors(or anything that has x,y,z props) or nested arrays.
 * Unless otherwise specified, returns a new Float32Array
 * @param arr the array of objects
 * @param type the type of array to return. If false, will return a regular array. Can specify array with shorthand ie "float32"
 * @returns {*}
 */
export function flattenArray(arr,type="float32"){

    let flat = [];
    if(arr[0].hasOwnProperty('x')){

        let len = arr.length;
        for(var i = 0; i < len; ++i){

            flat.push(arr[i].x,arr[i].y,arr[i].z);
        }
    }else if(arr instanceof Array){
        let len = arr.length;
        for(var i = 0; i < len; ++i){
            flat.push(arr[i][0],arr[i][1],arr[i][2]);
        }
    }else{
        console.error('flattenArray() param is not a Vector or a array');
        return false;
    }

    if(type !== false){
        switch(type){
            case "float32":
                return new Float32Array(flat);
                break;

            case "uint16":
                return new Uint16Array(flat);
                break;
        }
    }else{
        return flat;
    }
}

/**
 * Does the same thing as flattenArray but with some differences.
 * a. Assumes theres a pre-made typed array which you pass in for the first parameter
 * b. skips the type checking and automatically assumes your array of objects has x/y/z props
 * @param buffer
 * @param arr
 * @returns {*}
 */
export function fastFlattenArray(buffer,arr){

    let olen = arr.length;
    for(var i = 0; i < olen;++i){
        buffer[i * 3] = arr[i].x;
        buffer[i * 3 + 1] = arr[i].y;
        buffer[i * 3 + 2] = arr[i].z;

    }
    return buffer;
}

/**
 * Normalizes a vector to values between 0 and 1
 * @param vector the vector to normalize
 * @returns {*[]}
 */
export function normalize(vector){
    if(vector.hasOwnProperty("x")){
        let x = vector.x;
        let y = vector.y;
        let z = vector.z;

        let len = x * x + y * y + z * z;
        if(len > 0){
            len = 1 / Math.sqrt(len);
            x *= len;
            y *= len;
            z *= len;
        }

        return {
            x:x,
            y:y,
            z:z
        }
    }else if(vector instanceof  array){
        let x = vector[0];
        let y = vector[1];
        let z = vector[2];

        let len = x * x + y * y + z * z;
        if(len > 0){
            len = 1 / Math.sqrt(len);
            x *= len;
            y *= len;
            z *= len;
        }

        return [x,y,z];
    }
}

export function constrain(x,a,b){
    if(x < a){
        return a;
    }else if(b < x ){
        return b;
    }else {
        return x;
    }
}