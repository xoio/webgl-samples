const mat4 = require('gl-mat4');
const Vector = require('./Vector');

class Matrix4 {
    constructor(){
        this.matrix = mat4.create();
        mat4.identity(this.matrix);

        return this;
    }

    makePerspective(fov,aspect,near,far){
        this.matrix = mat4.perspective(this.matrix,fov,aspect,near,far);
    }

    /**
     * Translates the matrix along a vector.
     *
     * @param vec Either a Vector object or an array.
     * @returns {boolean}
     */
    translate(vec){
        mat4.translate(this.matrix,this.matrix,vec);
    }

    rotate(){
        mat4.rotate(this.matrix,this.matrix,degToRad(0),[1,1,0]);

        function degToRad(degrees) {
            return degrees * Math.PI / 180;
        }

    }

    /**
     * Return the raw matrix;
     * @returns {*}
     */
    get(){
        return this.matrix;
    }

}

export default Matrix4;