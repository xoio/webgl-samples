const createBuffer = require('gl-buffer');
const glslify = require('glslify');
const createVao = require('gl-vao');
const createShader = require('gl-shader');
const pack = require('array-pack-2d');

import {randomRange,randomInt,randVec3,normalize,fastFlattenArray,flattenArray} from "./math/Core"
import Matrix4 from "./math/Matrix4"
import Vector from "./math/Vector"

class Ribbon {
    constructor(gl){
        this.gl = gl;
        this.length = 40;
        this.modelMatrix = new Matrix4();
        this.head = new Vector();
        this.tail = new Vector();

        this.velocity = new Vector();
        this.speed = randomRange(5,20);
        this.ribbonWidth = randomRange(2,12);
        this.noiseId = Math.random() * noiseSeparation;

        //make shader
        this.shader = createShader(gl,glslify('./shaders/rvertex.glsl'),glslify('./shaders/rfragment.glsl'));

        //make vao and buffers
        this.vao = createVao(gl);
        this.indexBuffer = this._emptyBuffer(Uint16Array,gl.ELEMENT_ARRAY_BUFFER);
        this.positionBuffer = this._emptyBuffer();
        this.texCoordBuffer = this._emptyBuffer();


        this.shader.attributes.position.location = 0;
        this.modelMatrix.translate([0,0.0,-8.0]);



        this._buildMesh();
        this._reset();


        return this;
    }

    update(field){
        this.tail.copy(this.head);

        gvec.copy(this.head).divideScalar(noiseScale);
        let v = field.lookup(this.velocity * Math.random());

        this.velocity.x = noise.noise4d(gvec.x, gvec.y, gvec.z, 0  + noiseTime + this.noiseId );
        this.velocity.y = noise.noise4d(gvec.x, gvec.y, gvec.z, 50 + noiseTime + this.noiseId );
        this.velocity.z = noise.noise4d(gvec.x, gvec.y, gvec.z, 100 + noiseTime + this.noiseId );

        //this.velocity.x = noise.noise4d(gvec.x, gvec.y, gvec.z, 0  + noiseTime + this.noiseId ) * this.speed * ribbonSpeed;
        //this.velocity.y = noise.noise4d(gvec.x, gvec.y, gvec.z, 50 + noiseTime + this.noiseId ) * this.speed * ribbonSpeed;
        //this.velocity.z = noise.noise4d(gvec.x, gvec.y, gvec.z, 100+ noiseTime + this.noiseId ) * this.speed * ribbonSpeed;

        //this.velocity.x = noise.noise4d(gvec.x, gvec.y, gvec.z, 0  + noiseTime + this.noiseId ) * this.speed * ribbonSpeed;
        //this.velocity.y = noise.noise4d(gvec.x, gvec.y, gvec.z, 50 + noiseTime + this.noiseId ) * this.speed * ribbonSpeed;
        //this.velocity.z = noise.noise4d(gvec.x, gvec.y, gvec.z, 100+ noiseTime + this.noiseId ) * this.speed * ribbonSpeed;

        this.head.add(this.velocity);

        //reset if Out Of Bounds
        if (this.head.x > BOUNDS || this.head.x < -BOUNDS ||
            this.head.y > BOUNDS || this.head.y < -BOUNDS ||
            this.head.z > BOUNDS || this.head.z < -BOUNDS ) {

           this.velocity.multiplyScalar(-1);
        }

        tangent.subVectors(this.head,this.tail).normalize();

        gvec.crossVectors( tangent, up ).normalize();
        normal.crossVectors( tangent, gvec );
        normal.multiplyScalar(this.ribbonWidth);

//shift each 2 verts down one posn
//e.g. copy verts (0,1) -> (2,3)
        for ( var i = this.length - 1; i > 0; -- i ) {
            this.vertices[i*2].copy(this.vertices[(i-1)*2]);
            this.vertices[i*2+1].copy(this.vertices[(i-1)*2+1]);
        }

//populate 1st 2 verts with left and right edges
        this.vertices[0].copy(this.head).add(normal);
        this.vertices[1].copy(this.head).subtract(normal);


        this._updateBuffers();
    }

    draw(camera){

        this.shader.bind();

        this.shader.uniforms.projectionMatrix = camera.projection;
        this.shader.uniforms.viewMatrix = camera.view;
        this.shader.uniforms.modelMatrix = this.modelMatrix.get();

        this.vao.bind();
        this.vao.draw(this.gl.TRIANGLES,this.vertices.length);
        this.vao.unbind();

    }


    _buildMesh(){
        this.vertices = [];
        let indices = [];
        this.texCoords = [];

        for(let i = 0; i < this.length; ++i){
            this.vertices.push(new Vector(),new Vector());
        }

        for(let j = 0;j < this.length; ++j){
            indices.push(j * 2,j * 2 + 1,j * 2 + 2);
            indices.push(j * 2 + 1, j * 2 + 3, j * 2 + 2);
        }
        this.indexBuffer.update(new Uint16Array(indices));
        //console.log(this.vertices);
        this.vertBuffer = new Float32Array(this.vertices.length * 3);

    }

    _updateBuffers(){



        this.positionBuffer.update(fastFlattenArray(this.vertBuffer,this.vertices));
        //this.positionBuffer.update(flattenArray(this.vertices));
        this.vao.update([
            {
                buffer:this.positionBuffer,
                size:3
            }
        ],this.indexBuffer)
    }

    _reset(){

        if(Math.random() < clumpiness){
            let emitterId = randomInt(0,EMITTER_COUNT-1);
            let tvec = emitters[emitterId].add(randVec3(startRange));
            this.head.add(tvec);
        }else{
            this.head.copy(randVec3(BOUNDS));
        }

        this.tail.copy(this.head);


        for(let i = 0; i < this.length; ++i){
            this.vertices[i * 2].copy(this.head);
            this.vertices[i * 2 + 1].copy(this.head);
        }


        this._updateBuffers();


    }

    _emptyBuffer(arrayType, type) {
        let gl = this.gl;
        arrayType = arrayType || Float32Array
        return createBuffer(gl, new arrayType(), type || gl.ARRAY_BUFFER, gl.DYNAMIC_DRAW)
    }
}

export default Ribbon;