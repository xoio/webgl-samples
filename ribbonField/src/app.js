import {clear,enable} from "./glut"
import Global from "./Global"
import Ribbon from "./Ribbon"
import TestObject from "./TestObject"
import FlowField from "./FlowField"
const createCamera = require('perspective-camera')
const raf = require('raf');
const createTexture = require('gl-texture2d')
const baboon = require('baboon-image');

const gl = require('webgl-context')({
    width:window.innerWidth,
    height:window.innerHeight
});

document.body.appendChild(gl.canvas);

var stats = new Stats();

// align top-left
stats.domElement.style.position = 'absolute';
stats.domElement.style.left = '0px';
stats.domElement.style.top = '0px';
stats.domElement.style.zIndex = 888;
document.body.appendChild(stats.domElement)
////// setup camera ////////
var camera = createCamera({
    fov:Math.PI / 4,
    near:1,
    far:10000.0,
    viewport:[0,0,window.innerWidth,window.innerHeight]
})
camera.lookAt([0,0,0]);
camera.translate([0,0,2000]);

////// add items////////////
var field = new FlowField();
var ribbons = [];
var numRibbons = 500;
for(var i = 0; i < numRibbons;++i){
    ribbons.push(new Ribbon(gl));
}

var time = 0.0
raf(function tick(dt){
    time += dt / 1000;
    clear(gl);
    enable(gl.DEPTH_TEST);
    noiseTime += noiseSpeed;
    camera.update();

    stats.update();

    for(var i = 0; i < numRibbons;++i){
        ribbons[i].update(field);
        ribbons[i].draw(camera);
    }


    raf(tick);
});
