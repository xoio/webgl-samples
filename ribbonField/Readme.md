# ribbontrails

Experiment in [stack.gl](http://stack.gl/) and Ribbon making

## Overview

Based on [Felix Turner's](https://www.airtightinteractive.com/) [experiment](http://airtightinteractive.com/demos/js/noise-ribbons). Mostly the same but instead of simply using the Three.Geometry class, I wanted to
give myself a bit of a challenge and reconstruct everything from (mostly) raw webgl.

It's also written using the excellent set of libraries part of the [stack.gl](http://stack.gl) family

## Setup
* `npm install`
* `npm` start to begin compiling the JS
*  you'll need to run a server as well. If you happen to be on OSX, run `cd public`, then `python -m SimpleHTTPServer <optional port number>` to start a server on port 8000(or whatever port you specify). 