var UglifyJS = require('uglify-js');
var fs = require('fs');
var mode = "dev";
var args = process.argv.slice(2);

/**
 * Builds libs.
 * @param compress if compress is not undefined, will minify libs
 */
function buildLibs(_compress){
    if(_compress === undefined){
        _compress = false;
    }

    var libs = fs.readdirSync(__dirname + "/libs");
    var fileContents = [];

    //build absolute paths to libs
    libs = libs.map(function(item){
        return __dirname + "/libs/" + item;
    });

    /**
     * If compress is false, we just want to concat libs
     */
    if(_compress === false){

        libs.forEach(function(item){
            var contents = fs.readFileSync(item,"utf8");
            fileContents.push(contents);
        });

        fs.writeFile(__dirname + "/public/js/libs.js",fileContents.join("\n"),function(err){
            if(err){
                console.log("problem writing file");
            }
        })
    }else{
        var result = UglifyJS.minify(libs,{
            mangle:false,
            beautify:true
        })

        fs.writeFile(__dirname + "/public/js/libs.min.js",result.code,function(err){
            if(err){
                console.log("problem writing file");
            }
        })
    }




}

/**
 * Run the appropriate set of commands based on script args.
 * Default is dev
 */
switch (args[0]){
    case "dev":
        buildLibs()
        break;


    case "prod":
        buildLibs({})
        break;

    default:
        buildLibs();
        break;
}
